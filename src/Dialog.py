from typing import List, Tuple
from collections import namedtuple
Turn = namedtuple('Turn', ['user', 'agent','slots'], verbose=False)
Turns = List[Turn]


class Dialog:
    def __init__(self):
        self.turns = []
        self.i = -1

    def add_turn(self, turn: Turn):
        self.turns.append(turn)

    def __len__(self):
        return len(self.turns)

    def __iter__(self):
        return self

    def __next__(self):
        self.i += 1
        if len(self.turns) == self.i:
            raise StopIteration
        else:
            return self.turns[self.i]
