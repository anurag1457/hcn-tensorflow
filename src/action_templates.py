from typing import List, Dict, Tuple
from src.Context import Context
from queue import PriorityQueue
import sqlite3

def context_to_string(context: Context, slots: Dict[str, str]):
    ret=" "
    if context.properties["food"] is not None:
        ret += "that serves "+ context.properties["food"] +" food "
    if context.properties["area"] is not None:
        ret += "in the " +context.properties["area"] +" part of town"
    if context.properties["pricerange"] is not None:
        ret += "in the " +context.properties["pricerange"] +" pricerange"
    return ret

def restaurant_to_string(context: Context, slots: Dict[str, str]):
    restaurant = context.get_next_best_restaurant()
    pass

def hello_stmt(context: Context, slots: Dict[str, str]) -> str:
    return "hello , welcome to the cambridge restaurant system . you can ask for restaurants by area , price range or food type . how may i help you ?"

def welcome_stmt(context: Context, slots: Dict[str, str]) -> str:
    return "you are welcome"

def find_pricerange(context: Context, slots: Dict[str, str]) -> str:
    pass

def find_address(context: Context, slots: Dict[str, str]) -> str:
    pass

def find_phonenumber(context: Context, slots: Dict[str, str]) -> str:
    pass

def canthearyou(context: Context, slots: Dict[str, str]) -> str:
    return "sorry, i can't hear you"

def ask_food(context: Context, slots: Dict[str, str]) -> str:
    return "what kind of food would you like ?"

def ask_area(context: Context, slots: Dict[str, str]) -> str:
    return "what area would you like ?"

def api_call(context: Context, slots: Dict[str, str]) -> str:
    #todo add results of query to queue
    pass

def get_next(context: Context, slots: Dict[str, str]) -> str:
    if context.is_queue_empty():
        return "i am sorry but there is no other restaurant " + context_to_string(context, slots)
    else:
        #todo pop queue
        return context.properties["name"]+" is a great restaurant "+context_to_string(context, slots)