import queue
from typing import List, Tuple, Dict

class Context(object):
    def __init__(self, resto_name=None, resto_food=None, resto_area=None, resto_pricerange=None):
        # self.resto_name = resto_name
        # self.resto_area = resto_area
        # self.resto_food = resto_food
        # self.resto_pricerange = resto_pricerange
        self.properties = {'name':resto_name,
                           'area': resto_area,
                           'food': resto_food,
                           'pricerange':resto_pricerange}

        self.queue = queue.PriorityQueue()
        return

    def flush_queue(self):
        self.queue = queue.PriorityQueue()
        return

    def change_slot(self, slot_type: str, value: str) -> None:
        self.properties[slot_type]=value
        self.flush_queue()
        return None

    def add_results_to_queue(self, results: List) -> None:
        for result in results:
            self.queue.put(result)
        return None

    def get_next_best_restaurant(self):
        if not self.queue.empty():
            return self.queue.get()
        else:
            return None

    def is_queue_empty(self) -> bool:
        return self.queue.empty()