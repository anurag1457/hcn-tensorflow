import logging
import tensorflow as tf
from tensorflow.python.ops.rnn_cell_impl import LSTMStateTuple, LSTMCell
import numpy as np
from typing import List, Tuple, Dict, Callable, Any
from src.Dialog import Dialog
from src.decorators import delimprint
import pickle
import sys
from src.Paths import PathDict

task_number = int(sys.argv[1])
if task_number ==5:
    import src.preprocess as preprocess
else:
    import src.preprocess_6 as preprocess
path_dict = PathDict(task_number)

class Params:
    word_emb_dim = 300
    num_context_features = 12
    num_actions = 16
    lstm_hidden_dim = 128
    max_grad_norm = 100
    num_epochs_train = 2


class HCNModel(object):

    def __init__(self, vocab_siz, max_seq_le):
        self.success = False
        try:
            assert isinstance(vocab_siz, int) and isinstance(
                max_seq_le, int)
            self.x = tf.placeholder(tf.int32, shape=[None, max_seq_le], name='X')
            self.x_bow = tf.placeholder(tf.int32, shape=[None, vocab_siz], name="BOW_VECTOR")
            self.num_turns = tf.placeholder(tf.int32, shape=[1], name="NUM_TURNS")
            self.prev_actions = tf.placeholder(tf.float32, shape=[None, Params.num_actions],
                                               name='PREV_ACTIONS')
            self.y = tf.placeholder(tf.float32, shape=[None], name='Y')
            self.seq_len = tf.placeholder(tf.int32, shape=[None], name="SEQ_LEN")

            self.y_1hot = tf.one_hot(tf.to_int32(self.y), Params.num_actions)

            self.embedding_matrix = tf.get_variable("EMB_MATRIX", shape=[vocab_siz, Params.word_emb_dim],
                                               trainable=False)
            '''TODO matrix must have zero vector for index 0'''

            self.initial_state_h = tf.zeros(shape=[1, Params.num_actions])
            self.initial_state_c = tf.zeros(shape=[1, Params.lstm_hidden_dim])
            word2vec_encodings = tf.nn.embedding_lookup(self.embedding_matrix, self.x, name="UTTERANCE_ENCODING")
            # shape : num_turns * max_seq_len * emb_dim

            sum_embeddings = tf.reduce_sum(word2vec_encodings, axis=1, name="SUM_EMBEDDINGS")
            # shape : num_turns * emb_dim

            sum_embeddings_transpose = tf.transpose(sum_embeddings)

            # ALL INPUTS NEED TO BE OF TYPE float32
            seq_len_float32 = tf.to_float(self.seq_len)

            '''
            assert arg1.shape[-1] == seq_len.shape[-1]
            '''
            avg_embedding = tf.transpose(tf.divide(sum_embeddings_transpose, seq_len_float32, name="AVG_EMBEDDINGS"))
            # shape : num_turns * emb_dim

            all_features = tf.concat([avg_embedding, tf.to_float(self.x_bow), self.prev_actions], axis=1)
            all_features_expanded = tf.expand_dims(all_features, axis=0)
            turn_level_lstm_cell = LSTMCell(num_units=Params.lstm_hidden_dim, num_proj=Params.num_actions)

            tuple_initial_state = LSTMStateTuple(self.initial_state_c, self.initial_state_h)
            # LSTMStateTuple is a named-tuple (by defn. read-only)

            with tf.variable_scope("FORWARD_LSTM"):
                outputs, state = tf.nn.dynamic_rnn(cell=turn_level_lstm_cell,
                                                   inputs=all_features_expanded,
                                                   sequence_length=self.num_turns,
                                                   dtype=tf.float32,
                                                   initial_state=tuple_initial_state)
            '''outputs :: [1,?,128 or 16]    state [1,128]    depending on whether we have a proj layer'''
            self.loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=self.y_1hot, logits=outputs,
                                                                               name="LOSS"))

            tvars = tf.trainable_variables()
            grads, _ = tf.clip_by_global_norm(tf.gradients(self.loss, tvars), Params.max_grad_norm)
            optimizer = tf.train.AdamOptimizer()
            pred = tf.argmax(tf.squeeze(outputs, axis=[0]), 1)
            self.acc = tf.reduce_sum(tf.to_int32(tf.equal(pred, tf.to_int64(self.y))))

            self.train_op = optimizer.apply_gradients(zip(grads, tvars))
            self.success = True
        except AssertionError:
            self.success = False
            logging.exception("INITIALIZE Stats CORRECTLY. GRAPH CREATION FAILED")
        except:
            self.success = False
            logging.exception("GRAPH CREATION FAILED")
        finally:
            return

    def __call__(self, session: tf.Session, feed_dictionary) -> List[Any]:
        results = session.run(fetches=[self.train_op, self.loss], feed_dict=feed_dictionary)
        return results

    def __del__(self):
        msg = "MODEL CREATION FAILED" if not self.success else "DELETING MODEL TO CLEAR MEMORY"
        print(msg)


def get_id2v(vocab, w2v):
    emb_matrix = np.zeros((len(vocab), Params.word_emb_dim), dtype=np.float32)
    for word in vocab:
        if word.lower() in w2v:
            idx = vocab[word]
            emb_matrix[idx, :] = w2v[word]
    return emb_matrix


def load_saved_stuff():
    with open(path_dict['vocab'],'rb') as f:
        vocab = pickle.load(f)
    with open(path_dict['emb'], 'rb') as f:
        w2v = pickle.load(f)
    w2v[preprocess.OOVword] = np.zeros((300,))
    return w2v,vocab


def prepare_to_run(flag : str):
    if flag=="train":
        dialogs, max_turns = preprocess.load_data(flag)#todo unpack
        max_seq_len = 50 #TODO do not harcode
        w2v, vocab = preprocess.load_w2v(dialogs)
        obj = max_turns, len(vocab), max_seq_len
        id2v = get_id2v(vocab, w2v)
        with open(path_dict['vocab'],'wb') as f:
            pickle.dump(vocab,f)
    else:
        dialogs, _ = preprocess.load_data(flag)#todo unpack
        max_seq_len = 50
        max_turns = 27 #TODO load_from_file
        w2v,vocab = load_saved_stuff()
        obj = max_turns, len(vocab), max_seq_len
        id2v = get_id2v(vocab, w2v)
    return dialogs, id2v, vocab, obj

def fst(arg : Tuple):
    assert isinstance(arg, tuple) and len(arg)==2
    return arg[0]

def snd(arg : Tuple):
    assert isinstance(arg, tuple) and len(arg)==2
    return arg[1]

def extend_list(utterance : List[int], max_len : int)-> List[int]:
    length = len(utterance)
    assert length <= max_len

    diff = max_len - length
    for _ in range(diff):
        utterance.append(0)
    return utterance

def convert_dialog_to_arrs(dialog : Dialog, vocab : Dict[str, int], response_mapper : Callable[[str], int ], max_len : int) -> Dict:
    def convert_word_to_id(word: str) -> int:
        try:
            return vocab[word]
        except KeyError:
            return 0
    xs=[]
    ys = []
    seq_lens = []
    x_bows = []
    prev_actions= [[0 for _ in range(Params.num_actions)]]
    for turn in dialog:
        bow = np.zeros(shape=(len(vocab),), dtype=np.int32)
        words = turn.user.split()
        for word in words:
            bow[vocab[word]] = 1
        ids = [x for x in map(convert_word_to_id, words)]
        ext_ids = extend_list(ids, max_len)
        xs.append(ext_ids)
        y = response_mapper(turn.agent)
        ys.append(y)
        seq_lens.append(len(ids))
        x_bows.append(bow)
        action = np.zeros((Params.num_actions,))
        action[y]=1.
        prev_actions.append(action)
    assert len(xs) == len(ys) ==len(seq_lens) == len(prev_actions) - 1
    return {'xs' : np.array(xs), 'ys' : np.array(ys), 'seq_lens' : np.array(seq_lens),
            'num_turns':np.array([len(dialog)]), 'x_bows' : np.array(x_bows),
            'prev_actions': np.array(prev_actions[:-1])}


@delimprint
def printer(**kwargs):
    for kwarg in kwargs:
        print("%s is %f" % (kwarg, kwargs[kwarg]))


def train_epochs(sess, saver, a, dialogs_, vocab, max_seq_len_):
    max_acc = 0.
    for k in range(Params.num_epochs_train):
        printer(EPOCH_NUMBER=k)
        total_acc = 0.
        for dialog_ in dialogs_:
            feed = convert_dialog_to_arrs(dialog_, vocab, preprocess.response_to_action, max_seq_len_)
            feed_dict = {a.x: feed['xs'],
                         a.x_bow: feed['x_bows'],
                         a.num_turns: feed['num_turns'],
                         a.prev_actions: feed['prev_actions'],
                         a.y: feed['ys'],
                         a.seq_len: feed['seq_lens']}
            # _, loss = a(sess, feed_dict)
            loss, acc, _ = sess.run([a.loss, a.acc, a.train_op], feed_dict)
            printer(acc=float(acc) / len(dialog_.turns), loss=loss)
            total_acc += acc
        if total_acc > max_acc:
            max_acc = total_acc
            saver.save(sess, path_dict['save_file'])


def run_epoch(flag_ : str, sess, a : HCNModel, dialogs_, vocab, max_seq_len_):
    to_fetch=[a.loss, a.acc]
    if flag_ == "train":
        to_fetch.append(a.train_op)
    total_acc= 0
    for dialog_ in dialogs_:
        feed = convert_dialog_to_arrs(dialog_, vocab, preprocess.response_to_action, max_seq_len_)
        feed_dict = {a.x: feed['xs'],
                     a.x_bow: feed['x_bows'],
                     a.num_turns: feed['num_turns'],
                     a.prev_actions: feed['prev_actions'],
                     a.y: feed['ys'],
                     a.seq_len: feed['seq_lens']}
        loss, acc, _ = sess.run(to_fetch, feed_dict)
        printer(acc=float(acc) / len(dialog_.turns), loss=loss)
        total_acc += acc
    return total_acc


if __name__ == "__main__":
    flag_ = 'test'
    # os.chdir("../")
    # print(os.getcwd())
    dialogs_, id2v, vocab, obj = prepare_to_run(flag_)
    max_turn_, vocab_size_, max_seq_len_ = obj
    print(max_turn_, vocab_size_, max_seq_len_)

    with tf.Session() as sess:
        with tf.variable_scope("model", reuse=None):
            a = HCNModel(vocab_size_, max_seq_len_)
        print("GRAPH CREATED")
        saver = tf.train.Saver()
        ckpt = tf.train.get_checkpoint_state(path_dict['save_path'], latest_filename='checkpoint')

        if ckpt and ckpt.model_checkpoint_path:
            saver.restore(sess,ckpt.model_checkpoint_path)
            print("LOADED MODEL FROM %s" % ckpt.model_checkpoint_path)
        else:
            if not flag_ =="train":
                print("CAN'T TEST WITHOUT UPLOADING A SAVED MODEL")
                exit(1)
            sess.run(tf.initialize_all_variables())
            sess.run(tf.assign(a.embedding_matrix, id2v))
        if flag_ =="train":
            train_epochs(sess, saver, a, dialogs_, vocab, max_seq_len_)
        elif flag_ =="test":
            if True:
                for dialog_ in dialogs_:
                    feed = convert_dialog_to_arrs(dialog_, vocab, preprocess.response_to_action, max_seq_len_)
                    feed_dict = {a.x: feed['xs'],
                                 a.x_bow: feed['x_bows'],
                                 a.num_turns: feed['num_turns'],
                                 a.prev_actions: feed['prev_actions'],
                                 a.y: feed['ys'],
                                 a.seq_len: feed['seq_lens']}
                    # _, loss = a(sess, feed_dict)
                    loss, _, acc = sess.run([a.loss, a.train_op, a.acc], feed_dict)
                    printer(acc= float(acc)/len(dialog_.turns), loss=loss)


        # for k in range(Params.num_epochs):
        #     printer(EPOCH_NUMBER=k)
        #     total_acc = 0.
        #     for dialog_ in dialogs_:
        #         feed = convert_dialog_to_arrs(dialog_, vocab, preprocess.response_to_action, max_seq_len_)
        #         feed_dict = {a.x: feed['xs'],
        #                      a.x_bow: feed['x_bows'],
        #                      a.num_turns: feed['num_turns'],
        #                      a.prev_actions: feed['prev_actions'],
        #                      a.y: feed['ys'],
        #                      a.seq_len: feed['seq_lens']}
        #         # _, loss = a(sess, feed_dict)
        #         loss, _, acc = sess.run([a.loss, a.train_op, a.acc], feed_dict)
        #         printer(acc= float(acc)/len(dialog_.turns), loss=loss)
        #         total_acc += acc
        #     if total_acc>max_acc:
        #         max_acc=total_acc
        #         saver.save(sess, path_dict['save_path'])


