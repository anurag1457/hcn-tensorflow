from typing import List, Dict, Tuple
import pickle
from src.Dialog import *
import numpy as np
from src.Paths import PathDict
from nltk.tokenize import word_tokenize
import os
import json
import re
from src.Context import Context
from sklearn.cluster import k_means



with open("options.txt") as f:
    task_number = int(f.readline().strip().split()[-1])

slot_regex = re.compile("\[([ a-zA-Z]+)\]")
OOVword = 'thiswordmustneveroccurinthedataset'
path_dict = PathDict(task_number)

def split_by_tab(arg: str)-> List[str]:
    return arg.split("\t")

def is_utterance(line: str)-> bool:
    return len(line.split("\t")) > 1

def not_contain_underscore(word: str) -> bool:
    return word.__contains__("_")

def extract_slots_from_string(arg: str)-> Dict:
    slots = {}
    matches = slot_regex.findall(arg)
    if(len(matches)) > 0:
        slots = {m.split()[0] : m.split()[1] for m in matches}
    return slots

def find_slots(turn: List[str])-> Dict:
    if (len(turn) > 2):
        slots = extract_slots_from_string(turn[2])
    else:
        slots = {}
    return slots

def load_data(flag: str) -> Tuple[List[Dialog],int]:
    dialogs = []
    max_turns = 0
    for dialog in dialog_generator(path_dict[flag]):
        temp = Dialog()
        turns = [x for x in map(split_by_tab, filter(is_utterance, dialog))]
        max_turns = max(max_turns, len(turns))

        for turn in turns:
            match = turn[0]
            agentUtterance = turn[1]
            slots = find_slots(turn)
            if match:
                temp.add_turn(Turn(match, agentUtterance,slots)) #todo fix warning and add slots too
            else:
                raise ValueError
        dialogs.append(temp)
    return dialogs, max_turns

def dialog_generator(filename: str) -> List[str]:
    with open(filename) as f:
        chat = []
        try:
            while True:
                line = f.readline()
                if line == '':
                    raise StopIteration
                if not line.__eq__("\n"):
                    chat.append(line.rstrip())
                else:
                    yield chat
                    chat = []
        except StopIteration as e:
            print(e)
            return
    return

def gen_vocab_from_data(dialogs) -> Dict[str, int]:
    vocab = {OOVword: 0}
    for dialog in dialogs:
        for turn in dialog.turns:
            user_words = turn.user.split()
            for word in user_words:
                if not vocab.__contains__(word):
                    vocab[word] = len(vocab)
            agent_words = turn.agent.split()
            for word in filter(not_contain_underscore,agent_words):
                if not vocab.__contains__(word):
                    vocab[word] = len(vocab)
    return vocab

def load_w2v(dialogs):
    w2v = pickle.load(open(path_dict['emb'],'rb'))
    vocab = gen_vocab_from_data(dialogs)
    w2v[OOVword] = np.zeros((300,))
    return w2v, vocab

def parse_kb(filename: str) -> Tuple[Dict, Dict]:
    kb = {}
    all_slot_values= {}
    for line in open(filename):
        _, name, slot_name, slot_value = line.lower().strip().split()
        if name in kb:
            kb[slot_name] = slot_value
        else:
            kb = {slot_name: slot_value}
        all_slot_values[slot_value] = slot_name
    return [kb, all_slot_values]

def parametrize_data(filename: str, reverse_ontology: Dict[str, str], parametrize_agent: bool):
    data = []

    def subst(line: str) -> str:
        present = []
        user, agent = line.split("\t")
        slots = ""
        for word in reverse_ontology:
            if word in line:
                present.append(word)
        for val in present:
            user = user.replace(val, '$' + reverse_ontology[val])
            slots += "[" + reverse_ontology[val] + " " + val +"] "
            if parametrize_agent:
                agent = agent.replace(val, '$' + reverse_ontology[val])

        return user+"\t"+agent.rstrip() +"\t"+slots+"\n"

    for line in open(filename,'r'):
        line = line.lower()
        if is_utterance(line):
            data.append(subst(line))
        elif line =="\n":
            data.append(line)
    return data




def process_state(action: int, context: Context, slots: Dict[str, str]) -> str:
    for slot in slots:
        if slot in context.properties:
            if slots[slot] == context.properties[slot]:
                continue
            else:
                context.change_slot(slot, slots[slot])
        else:
            context.properties[slot]=slots[slot]
    #todo think of a scenario where the above code might fail


    pass

#TODO convert all responses to lowercase
def response_to_action(response: str) -> int:
    if response.__contains__("you are welcome"):
        return 0
    elif response.__contains__(""):#TODO
        return 1
    elif response.__contains__("api_call"):
        return 2
    elif response.__contains__("welcome to the cambridge restaurant system"):
        return 3
    elif response.__contains__("what kind of food would you like"):
        return 4
    elif response.__contains__("phone number of"):
        return 5
    elif response.__contains__("would you like something in the cheap , moderate , or expensive price range?"):
        return 6
    elif response.__contains__("what part of town do you have in mind"):
        return 7
    elif response.__contains__("sure, ") and response.__contains__("is on"):
        return 8
    elif response.__contains__("i'm sorry but there is no restaurant serving"):
        return 9
    elif (response.__contains__("serves") or response.__contains__("serving")) and response.endswith("food"):
        return 10
    elif response.__contains__(""):#TODO moderate price range
        return 11
    elif response.__contains__(""):#TODO similar to 2
        return 12
    elif response.__contains__("you say you are looking for a restaurant in the"):
        return 13
    elif response.__contains__(""):#todo similar to 10
        return 14
    elif response.__contains__(""):#todo similar to 10
        return 15
    elif response.__contains__("are looking for a restaurant is that right"):
        return 16
    elif response.__contains__(""):#todo similar to 9 and 10
        return 17
    elif response.__contains__(""):
        return 18
    elif response.__contains__(""):
        return 19
    elif response.__contains__(""):
        return 20
    elif response.__contains__(""):
        return 21
    elif response.__contains__(""):
        return 22
    elif response.__contains__(""):
        return 23
    elif response.__contains__(""):
        return 24
    elif response.__contains__(""):
        return 25
    elif response.__contains__(""):
        return 26
    elif response.__contains__(""):
        return 27
    elif response.__contains__(""):
        return 28
    elif response.__contains__(""):
        return 29
    elif response.__contains__(""):
        return 30
    elif response.__contains__(""):
        return 31
    elif response.__contains__(""):
        return 32
    elif response.__contains__(""):
        return 33
    elif response.__contains__(""):
        return 34
    else:
        raise ValueError

def convert_to_bow(tokens: List[str], w2id: Dict[str, int]):
    pass
    vec= np.zeros((len(w2id,)))
    for token in tokens:
        vec[w2id[token]] = 1.
    return vec

if __name__=="__main__":
    agent_utterances = []
    vocab = {}
    for line in open(path_dict["train"]):
        tabsplit = line.split("\t")
        if is_utterance(line):
            agent_utterances.append(tabsplit[1])
            for word in tabsplit[1].split():
                if word not in vocab:
                    vocab[word] = len(vocab)

    with open("k-means.pkl","rb") as f:
        model = pickle.load(f)
    print(len(agent_utterances))
    labels = model[1].tolist()
    mapping = {x : y for x,y in zip(agent_utterances, labels)}
    print(len(mapping))
    sorted_mapping = sorted(mapping, key=mapping.get)
    f = open("mapping.txt","w")
    for key in sorted_mapping:
        f.write(key+'\t'+str(mapping[key])+"\n")
    f.close()

    #
    # vectors = []
    # for utterance in agent_utterances:
    #     vectors.append(convert_to_bow(utterance.split(),vocab))
    # arr = np.vstack(vectors)
    # model = k_means(arr, 58, init='k-means++', n_init=20, max_iter=1000)
    # with open("k-means.pkl","wb") as f:
    #     pickle.dump(model,f)


    # with open("reverse_ontology_noask.json") as f:
    #     rev = json.load(f)
    # for flag in ["train"]:
    #     data = parametrize_data(path_dict[flag],rev,True)
    #     with open("param_{}_noask.txt".format(flag),"w") as f:
    #         f.write("".join(data))

    # for flag in flags:
    #     data = []
    #     count = 0
    #     for line in open(path_dict[flag]):
    #         if line == "\n":
    #             data.append(line)
    #             continue
    #         utterances = line.split("\t")
    #         if(len(utterances) < 2):
    #             continue
    #         user, agent = line.split("\t")
    #         words = word_tokenize(user)
    #         for word in words:
    #             if word in slot_dict:
    #                 mod = True
    #                 count += 1
    #                 break
    #         parametrized_words = map(subst, words)
    #         parametrized_user = " ".join([word for word in parametrized_words])
    #         data.append("\t".join([parametrized_user, agent]))
    #
    #     with open("parametrized_"+flag+".txt", "w") as f:
    #         for datum in data:
    #             f.write(datum)