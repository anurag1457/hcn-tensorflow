import sqlite3
from typing import List, Tuple, Dict
from src.Paths import PathDict

with open("options.txt") as f:
    task_number = int(f.readline().strip().split()[-1])
path_dict = PathDict(task_number)


def record_generator(filename: str):
    f = open(filename)
    record = []

    while True:
        line = f.readline()
        if line=='':
            return
        elif not line == "\n":
            record.append(line.rstrip())
        else:
            yield record
            record = []

def add_record_to_table(record: List[str], cursor) -> None:
    pricerange=''
    location=''
    food = ''
    for feature in record:
        curr_name, curr_slot_type, curr_value = feature.split()

        if curr_slot_type == "R_cuisine":
            food = curr_value
        elif curr_slot_type == "R_location":
            location = curr_value
        elif curr_slot_type == "R_price":
            pricerange = curr_value
    name = curr_name
    address = name +"_address"
    post_code = name+"_post_code"
    phone = name +"_phone"
    record = (name, pricerange, location, food, address, post_code, phone)
    cursor.execute('INSERT INTO restaurants VALUES (?,?,?,?,?,?,?)',record)

    return

def txt_to_sql():
    conn = sqlite3.connect('babi6.db')
    curs = conn.cursor()
    curs.execute('''CREATE TABLE restaurants (name text, pricerange text,
     location text, food text, address text, post_code text, phone text)''')
    for record in record_generator(path_dict['kb']):
        add_record_to_table(record, curs)
        conn.commit()
    conn.close()
    return

if __name__ == "__main__":
    txt_to_sql()