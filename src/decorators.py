from typing import Callable,List

def delimprint(printer):
    def wrapper(**kwargs):
        printer(**kwargs)
        print("************************")
        return
    return wrapper