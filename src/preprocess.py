from typing import List, Dict, Tuple
import re
import pickle
from src.Dialog import *
import numpy as np
from src.Paths import PathDict
import sys

utterance_regex = re.compile("^[0-9]{1,3} resto_")
remove_number_regex = re.compile("^[0-9]{1,3} (.*)")

with open("task_number.txt") as f:
    task_number = int(f.readline().strip().split()[-1])

path_dict = PathDict(task_number)
OOVword = 'thiswordmustneveroccurinthedataset'


class GlobalState:
    data_loaded = False
    w2v_loaded = False


def dialog_generator(filename: str) -> List[str]:
    with open(filename) as f:
        chat = []
        try:
            while True:
                line = f.readline()
                if line == '':
                    raise StopIteration
                if not line.__eq__("\n"):
                    chat.append(line.rstrip())
                else:
                    yield chat
                    chat = []
        except StopIteration as e:
            print(e)
            return
    return


def get_else_empty(dictionary: Dict[str, List[str]], response: str) -> List[str]:
    if dictionary.__contains__(response[:5]):
        return dictionary[response[:5]]
    else:
        return []


def add_template(dictionary: Dict[str, List[str]], response: str):
    dictionary[response[:5]] = get_else_empty(dictionary, response) + [response]
    return


def split_by_tab(line: str) -> List[str]:
    return line.split("\t")


def is_utterance(line: str) -> bool:
    return utterance_regex.match(line) is None

def response_to_action(response: str) -> int:
    if response.startswith("api_call"):
        return 0
    elif response.startswith("what do you think of this option"):
        return 1
    elif response.startswith("i'm on it"):
        return 2
    elif response.startswith("which price range are"):
        return 3
    elif response.startswith("ok let me look into some options"):
        return 4
    elif response.startswith("any preference on a type of cuisine"):
        return 5
    elif response.startswith("hello what can i help you"):
        return 6
    elif response.startswith("how many people"):
        return 7
    elif response.startswith("great let me do the reservation"):
        return 8
    elif response.startswith("sure is there anything else to update"):
        return 9
    elif response.startswith("where should it be"):
        return 10
    elif response.startswith("is there anything i"):
        return 11
    elif response.startswith("you're welcome"):
        return 12
    elif response.startswith("here it is") and response.endswith("phone"):
        return 13
    elif response.startswith("here it is") and response.endswith("address"):
        return 14
    elif response.startswith("sure let me find an other"):
        return 15
    else:
        print(response)
        raise ValueError


def remove_leading_numbers(arg: str) -> re.match:
    return remove_number_regex.match(arg)

def load_data(flag: str):
    dialogs = []
    max_turns = 0
    for dialog in dialog_generator(path_dict[flag]):
        temp = Dialog()
        turns = [x for x in map(split_by_tab, filter(is_utterance, dialog))]
        max_turns = max(max_turns, len(turns))
        for turn in turns:
            match = remove_leading_numbers(turn[0])
            agentUtterance = turn[1]
            if match:
                temp.add_turn(Turn(match.group(1),agentUtterance))
            else:
                raise ValueError
        dialogs.append(temp)
    GlobalState.data_loaded = True
    return dialogs, max_turns


def not_contain_underscore(word: str) -> bool:
    return word.__contains__("_")

def gen_vocab_from_data(dialogs) ->Dict[str, int]:
    vocab = {OOVword: 0}
    for dialog in dialogs:
        for turn in dialog.turns:
            user_words = turn.user.split()
            for word in user_words:
                if not vocab.__contains__(word):
                    vocab[word] = len(vocab)
            agent_words = turn.agent.split()
            for word in filter(not_contain_underscore,agent_words):
                if not vocab.__contains__(word):
                    vocab[word] = len(vocab)
    return vocab

'''
def get_vocab(dialogs, flag):
    if flag == "train":
        return gen_vocab_from_data(dialogs)
    else:
        return
'''

def load_w2v(dialogs):
    w2v = pickle.load(open(path_dict['emb'],'rb'))
    vocab = gen_vocab_from_data(dialogs)
    w2v[OOVword] = np.zeros((300,))
    GlobalState.w2v_loaded = True
    return w2v, vocab
