class PathDict():
    def __init__(self,task_number):
        self._dictionary = {'train': 'param_train%d_noask.txt' % task_number,
         'test' : 'data/babi%d/test.txt' % task_number,
         'dev' :'data/babi%d/dev.txt' % task_number,
         'emb' : 'data/word2vec.pkl',
         'kb' : "data/babi%d/kb.txt" % task_number,
         'save_path' : "models%d/" % task_number,
         'vocab' : "data/vocab%d.pkl" % task_number,
         'save_file' : "models%d/model" % task_number}
        pass

    def __getitem__(self, item):
        try:
            ret = self._dictionary[item]
        except KeyError as k:
            ret = self._dictionary["unk"]
        finally:
            return ret